const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");


// Route for retrieving all order
router.get("/",(req,res)=>{
  const allOrder = auth.decode(req.headers.authorization).isAdmin;
  orderController.getAllOrder().then(resultFromController=>res.send(resultFromController))
});

// Route for create an orders
router.post("/order",auth.verify,(req,res)=>{
    const data = {
            userId: auth.decode(req.headers.authorization).id,
            //totalAmount: req.body.totalAmount,
            productId: req.body.productId,
            quantity: req.body.quantity
            
        }
        console.log(data)
    orderController.createOrder(data).then(resultFromController=>res.send(resultFromController));
    
});


 module.exports = router;