const Product = require("../models/Product");

const auth = require("../auth");


// Create/add a products
module.exports.addProduct = (data) => {
console.log(data);

	if(data.isAdmin){

		let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price
	});

	console.log(newProduct);
	return newProduct.save().then((product, error) => {

		if (error) {

			return false;

		} else {

			return newProduct;

		};

	});

	}else{
		return false;
	}
};

// Retrieve all active products
module.exports.getAllActive = () =>{
	return Product.find({isActive:true}).then(result=>{
		return result;
	})
}

// Retrieving all products
module.exports.getAllProducts = () =>{
	return Product.find({}).then(result=>{
		return result;
	})
}

// Retrieving single product
module.exports.getSingleProduct = (reqParams)=>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	});
};

// Update a product (admin only)
module.exports.updateProduct = (reqParams,reqBody) =>{

	let updatedProduct ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
		
		if(error){
			return false;
		// Product updated successfully
		}else{
			return "Product updated successfully";
		};
	});
};

// Archive a product
module.exports.archiveProduct = (reqParams, reqBody)=> {
	let productStatus1 = {
		isActive: reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, productStatus1).then((product, error)=> {
		if(error) {
			return false;
		} else {
			return Product.findById(reqParams.productId).then(result=> {
				return result;
			});
		}
	});
};


// Activate a product
module.exports.activateProduct = (reqParams, reqBody)=> {
	let productStatus2 = {
		isActive: reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, productStatus2).then((product, error)=> {
		if(error) {
			return false;
		} else {
			return Product.findById(reqParams.productId).then(result=> {
				return result;
			});
		}
	});
};


//Deleting a product
module.exports.deleteProduct = (productId) =>{

	return Product.findByIdAndRemove(productId).then((removedProduct,err)=>{
		if(err){
			console.log(err);
			return false;
		}else{
			return removedProduct;
		}
	})
}
