const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Product is required"]
	},
	description : {
		type: String,
		required:[true,"Description is required"] 
	},
	price : {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	orders : [
	{
		orderId:{
			type: String,
			required: [true,"orderId is required"]
		},
		quantity: {
			type: Number,
			default: 1
		}

	}
	]

});

module.exports = mongoose.model("Product",productSchema);



// const mongoose = require('mongoose');

// const productSchema = mongoose.Schema({
//     _id: mongoose.Schema.Types.ObjectId,
//     name: { 
//     	type: String, 
//     	required: true 
//     },
//     price: { 
//     	type: Number, 
//     	required: true 
//     },
//     description : {
// 		type: String,
// 		required:true 
// 	},
// 	isActive : {
// 		type: Boolean,
// 		default: true
// 	},
// 	createdOn : {
// 		type: Date,
// 		default: new Date()
// 	}
// });

// module.exports = mongoose.model('Product', productSchema);
